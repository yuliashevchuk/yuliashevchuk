package collectlaba;

public class Box {

    private double weight;
    private String name;


    public Box( String name,double weight) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Box{" +
                "weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

