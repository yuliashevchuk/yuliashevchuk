package collectlaba;

import java.util.*;

public class ColectionStudent {

    public static Set<Student> studentsWhichisFamale(Set<Student> students) {
        Set<Student> femaleList = new TreeSet<>();
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (student.getSex().equals( "girl" )) {
                femaleList.add( student );
                iterator.remove();

            }
        }
        return femaleList;
    }

    public static Set<Student> studentsWhichisMale(Set<Student> students) {
        Set<Student> maleList = new TreeSet<>();
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()) {
            Student student1 = iterator.next();
            if (student1.getSex().equals( "boy" )) {
                maleList.add( student1 );
                iterator.remove();

            }
        }
        return maleList;
    }

    public static void main(String[] args) {

        Set<Student> students = new TreeSet<>();
        Set<Student> boys = new TreeSet<>();
        Student boy1 = new Student( "Гавришків Олег", "boy" );
        Student boy2 = new Student( "Гут Назар", "boy" );
        Student boy3 = new Student( "Гой Олег", "boy" );
        Student boy4 = new Student( "Співак Максим", "boy" );
        Student boy5 = new Student( "Левицький Юрій", "boy" );
        Student boy6 = new Student( "Покровець Назар", "boy" );
        Student boy7 = new Student( "Крюков Євген", "boy" );
        Student boy8 = new Student( "Кріль Петро", "boy" );
        boys.add( boy1 );
        boys.add( boy2 );
        boys.add( boy3 );
        boys.add( boy4 );
        boys.add( boy5 );
        boys.add( boy6 );
        boys.add( boy7 );
        boys.add( boy8 );
//        boys.sort(Comparator.comparing(Student::getName));
        Set<Student> girls = new TreeSet<>();
        Student girl1 = new Student( "Пономаренко Марта", "girl" );
        Student girl2 = new Student( "Оссовська Тетяна", "girl" );
        Student girl3 = new Student( "Щадило Наталя", "girl" );
        Student girl4 = new Student( "Квятковська Настя", "girl" );
        Student girl5 = new Student( "Шевчук Юлія", "girl" );
        girls.add( girl1 );
        girls.add( girl2 );
        girls.add( girl3 );
        girls.add( girl4 );
        girls.add( girl5 );
//        girls.sort(Comparator.comparing(Student::getName));
        students.addAll( boys );
        students.addAll( girls );
//        Students.sort(Comparator.comparing(Student::getName));
        System.out.println( "Список групи:" + students + "\n" );
        Set<Student> femaleList = studentsWhichisFamale( students );
        System.out.println( "Список дівчат групи:" + femaleList + "\n" );
        Set<Student> maleList = studentsWhichisMale( students );
        System.out.println( "Список хлопців групи :" + maleList + "\n" );
    }
}
