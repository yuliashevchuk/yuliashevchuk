package collectlaba;

import java.util.*;

public class ColectionBox {
    public static List<Box> moveWright(List<Box> list) {
        List<Box> listsort = new ArrayList<>();
        Iterator<Box> iterator = list.iterator();
        while (iterator.hasNext()) {
            Box box = iterator.next();
            if (box.getWeight() > 300) {
                listsort.add( box );
                iterator.remove();
            }
        }
        return listsort;
    }
    public static void main(String[] args) {
        List<Box> list = new ArrayList<>();

        Box box1 = new Box( "matter",100 );
        Box box2 = new Box( "gift", 500 );
        Box box3 = new Box("note", 300 );
        Box box4 = new Box( "workbook",200 );
        list.add( box1 );
        list.add( box2 );
        list.add( box3 );
        list.add( box4 );
        System.out.println( list );
        List<Box> listsort = moveWright(list);
        System.out.println(listsort);



    }
}
