package collectlaba;

import java.util.*;

public class Month {
    public static void main(String args[]) {
        List<String> winter = new ArrayList<>();
        winter.add(0, "December");
        winter.add(1, "January");
        winter.add(2, "February");
        System.out.println("Зимові місяці:");
        for (Object o : winter)
            System.out.println(o);
        List<String> sprint = new ArrayList<>();
        sprint.add(0, "March");
        sprint.add(1, "April");
        sprint.add(2, "May");
        System.out.println("Весняні місяці:");
        for (Object е : sprint)
            System.out.println(е);
        List<String> summer = new ArrayList<>();
        summer.add(0, "June");
        summer.add(1, "July");
        summer.add(2, "August");
        System.out.println("Літні місяці:");
        for (Object q : summer)
            System.out.println(q);
        List<String> autumn = new ArrayList<>();
        autumn.add(0, "September");
        autumn.add(1, "October");
        autumn.add(2, "November");
        System.out.println("Осінні місяці:");
        for (Object p : autumn)
            System.out.println(p);
        List<String> month = new ArrayList<>();
        month.addAll(winter);
        month.addAll(sprint);
        month.addAll(summer);
        month.addAll(autumn);
        System.out.println("\n");
        System.out.println("Місяці:");
        for (Object a : month)
            System.out.println(a);


    }


}

